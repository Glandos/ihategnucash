I hate GnuCash
==============

OK. Not really. It is just a mix between [ihatemoney](https://github.com/spiral-project/ihatemoney) and [http://gnucash.org/](GnuCash).

The only goal is to import a budget into your GnuCash file, in a single, splitted transaction.
Currently, your GnuCash file **must** be in SQLite format.

Requirements
------------------
- attrs
- click
- colored (optional)
- piecash
- requests

Yes, this a lot of requirements for a small projects, but I'm currently playing with some features. It *is* fun :)


Example
-----------
```
python ihategnucash.py --main-member-name Glandos --main-account-name 'Compte courant' test_sqlite3.gnucash http://budget.example.com/api/projects/shopping
```
