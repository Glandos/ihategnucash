#!./bin/python

import difflib
import re
import readline

from decimal import Decimal
from collections import OrderedDict
from urllib.parse import urlparse
from itertools import cycle

import requests
import attr
import click

from piecash import *

try:
    from colored import fg, bg, attr as att
except:
    # No colors
    fg = bg = att = lambda _: ""

MEMBERS_COLORS = cycle(
    [
        fg("light_yellow_3"),
        fg("indian_red_1a"),
        fg("pale_green_1b"),
        fg("misty_rose_3"),
        fg("grey_93"),
    ]
)

QUANTIZER = Decimal('0.001')

def existing_member_validator(instance, attr, id):
    if id in BillMember.EXISTING_MEMBERS:
        raise ValueError("Member is already there")
    BillMember.EXISTING_MEMBERS[id] = instance


def to_decimal(x):
    return Decimal(str(x))


@attr.s
class BillMember:
    id = attr.ib(validator=existing_member_validator)
    name = attr.ib()
    activated = attr.ib()
    weight = attr.ib(converter=to_decimal)
    balance = attr.ib(converter=to_decimal)

    color = attr.ib(default=attr.Factory(lambda: next(MEMBERS_COLORS)))
    is_main = attr.ib(default=False, cmp=False, hash=False)

    # Build a map of existing members to avoid troubles
    EXISTING_MEMBERS = {}


def build_members(members_json):
    # Don't put it in the map, the validator will do.
    # Can't use get() with a default, because the object is created and validated in the call.
    return [
        BillMember.EXISTING_MEMBERS[member["id"]]
        if member["id"] in BillMember.EXISTING_MEMBERS
        else BillMember(**member)
        for member in members_json
    ]


@attr.s
class BillGroup:
    criteria = attr.ib()
    bills = attr.ib(default=attr.Factory(list))


@attr.s
class Bill:
    id = attr.ib()
    budget = attr.ib(repr=False)
    what = attr.ib()
    payer_id = attr.ib()
    owers = attr.ib(converter=build_members)
    amount = attr.ib(converter=to_decimal)
    date = attr.ib()
    creation_date = attr.ib()
    original_currency = attr.ib()
    converted_amount = attr.ib()

    payer = attr.ib(
        default=None, validator=lambda instance, _2, _3: instance._init_payer()
    )
    group = attr.ib(default=None)
    matched = attr.ib(default=False)
    external_link = attr.ib(default=None)

    def _init_payer(self):
        self.payer = self.budget.get_member(self.payer_id)


@attr.s
class Budget:
    id = attr.ib()
    name = attr.ib()
    contact_email = attr.ib()
    members = attr.ib(converter=build_members)
    password = attr.ib()
    url = attr.ib()
    logging_preference = attr.ib()
    default_currency = attr.ib()

    bills = attr.ib(default=None, init=False)
    main_member = attr.ib(default=None, init=False)

    def get_member(self, id):
        return next((member for member in self.members if member.id == id), None)

    def read_bills(self):
        print("Reading bills…")
        bills = requests.get(self.url + "/bills", auth=(self.id, self.password)).json()
        return [Bill(budget=self, **bill) for bill in bills]

    def get_bills(self):
        if self.bills is None:
            self.bills = self.read_bills()
            self.bills.sort(key=lambda bill: bill.date)
        return self.bills

    def set_main_member(self, member_name):
        for member in self.members:
            if member.name == member_name:
                member.is_main = True
                self.main_member = member
                return member


@attr.s
class AccountMatcher:
    book = attr.ib()
    is_init = attr.ib(default=False, init=False)
    account_map = attr.ib(init=False)

    def init_map(self):
        if self.is_init:
            return
        self.account_map = {split.memo: split.account for split in self.book.splits}

        self.is_init = True

    def get_best_matches(self, description, n=3):
        self.init_map()
        best_matches = difflib.get_close_matches(
            description, self.account_map.keys(), n
        )
        best_accounts = [self.account_map[match] for match in best_matches]
        # Remove duplicates. This is slow, but we have a small set.
        return list(OrderedDict.fromkeys(best_accounts))


@attr.s
class AccountInput:
    book = attr.ib()
    is_init = attr.ib(default=False, init=False)
    accounts = attr.ib(init=False)
    readline_matches = attr.ib(init=False)

    def init(self):
        if self.is_init:
            return
        self.accounts = self.book.accounts
        self.accounts.fallback = lambda **kwargs: None

        # Init readline, just once
        for account in self.accounts:
            readline.add_history(account.fullname)

        readline.parse_and_bind("tab: complete")
        readline.set_completer_delims("")
        self.is_init = True

    @staticmethod
    def get_completions(book, text):
        parts = text.split(":")
        parents = [book.root_account]
        for part in parts:
            parents = [
                account
                for parent in parents
                for account in parent.children
                if re.match(part, account.name, re.I)
            ]
        return parents

    def readline_complete(self, text, state):
        if state == 0:
            self.readline_matches = AccountInput.get_completions(self.book, text)

        try:
            account = self.readline_matches[state]
            return account.fullname + (":" if account.children else "")
        except:
            return None

    def ask(self, best_accounts):
        self.init()
        readline.set_completer(self.readline_complete)

        if best_accounts:
            print("Best account matches:")
        for index, account in enumerate(best_accounts, start=1):
            print("  {} - {}".format(index, account.fullname))

        account = None
        while not account:
            choice = input("Account: ")
            choice = "1" if not choice else choice
            try:
                index = int(choice)
                account = best_accounts[index - 1]
            except (ValueError, IndexError):
                pass

            if not account:
                choice = choice[:-1] if choice.endswith(":") else choice
                # Try to get the account
                account = self.accounts(name=choice) or self.accounts(fullname=choice)
            if not account:
                print("Invalid choice")
        return account


@attr.s
class BudgetPrinter:
    budget = attr.ib()
    book = attr.ib()

    def show_budget_summary(self):
        print(
            """Budget {name} with {members}. Balance is:
{balance}""".format(
                name=self.budget.name,
                members=self.get_members_string(self.budget.members),
                balance="\n".join(
                    [
                        "- {member} {member_balance} €".format(
                            member=self.get_members_string([member]),
                            member_balance=self.get_colored_number(
                                member.balance
                            ),
                        )
                        for member in self.budget.members
                    ]
                ),
            )
        )

    def get_colored_number(self, number):
        return "{color}{number}{reset}".format(
            color=fg("green") if number >= 0 else fg("red"),
            number=number,
            reset=att("reset"),
        )

    def get_members_string(self, members):
        return ", ".join(
            [
                "{color}{main}{name}{reset}".format(
                    name=member.name,
                    color=member.color,
                    main=bg("dark_gray") if member.is_main else "",
                    reset=att("reset"),
                )
                for member in members
            ]
        )

    def print_bill_summary(self, bill, index, total):
        index_string = "{index}/{total}".format(index=index + 1, total=total)
        print(
            """{index} - {bill.date}: {amount} € from {payer} to {owers}
{indent}{what_color}{bill.what}{reset}""".format(
                index=index_string,
                bill=bill,
                owers=self.get_members_string(bill.owers),
                payer=self.get_members_string([bill.payer]),
                amount=self.get_colored_number(bill.amount),
                indent=" " * (len(index_string) + 3),
                reset=att("reset"),
                what_color=fg("deep_sky_blue_1"),
            )
        )

    def print_bill_group(self, group):
        print(
            "Grouped bills for {what_color}{group.criteria}{reset}".format(
                group=group, what_color=fg("deep_sky_blue_1"), reset=att("reset")
            )
        )
        for bill in group.bills:
            print(
                """ - {bill.date}: {amount} € from {payer} to {owers}{reset}""".format(
                    bill=bill,
                    owers=self.get_members_string(bill.owers),
                    payer=self.get_members_string([bill.payer]),
                    amount=self.get_colored_number(bill.amount),
                    reset=att("reset"),
                    what_color=fg("deep_sky_blue_1"),
                )
            )


@attr.s
class IHateGnuCash:
    gnucash_file = attr.ib(default=None)
    url = attr.ib(default=None)
    password = attr.ib(default=None)
    main_member_name = attr.ib(default=None)
    main_account_name = attr.ib(default=None)

    budget = attr.ib(default=None)
    book = attr.ib(default=None)
    account_matcher = attr.ib(default=None)
    account_input = attr.ib(default=None)
    main_account = attr.ib(default=None)
    transaction = attr.ib(default=None)
    transaction_description = attr.ib(default=None)
    total_transaction_amount = attr.ib(default=Decimal(0))

    printer = attr.ib(default=None)

    def prepare_readline(self, choices=None):
        def readline_complete(text, state):
            try:
                return [
                    completion for completion in choices if completion.startswith(text)
                ][state]
            except:
                return None

        readline.set_completer(readline_complete)

    def ask_missing(self):
        readline.parse_and_bind("tab: complete")

        if self.url is None:
            self.url = click.prompt("Project URL")
        if self.password is None:
            self.password = click.prompt("Password", hide_input=True)

        if self.budget is not None:
            if self.main_member_name is None:
                member_names = [member.name for member in self.budget.members]
                self.prepare_readline(member_names)
                self.main_member_name = click.prompt(
                    "Your name in the budget", type=click.Choice(member_names)
                )

        if self.book is not None:
            if self.main_account_name is None:
                self.account_input.init()
                readline.set_completer(self.account_input.readline_complete)
                self.main_account_name = click.prompt("Your GnuCash's account")
            if self.transaction_description is None:
                readline.set_completer(None)
                self.transaction_description = click.prompt(
                    "New transaction description", default="Budget balance"
                )

    def read_budget(self):
        print("Reading budget informations…")
        name = urlparse(self.url).path.split("/")[-1]
        budget = requests.get(self.url, auth=(name, self.password)).json()
        self.budget = Budget(**budget, password=self.password, url=self.url)

    def group_bills(self):
        grouped = {}
        for bill in self.budget.get_bills():
            grouped.setdefault(bill.what, []).append(bill)

        for reason, bills in grouped.items():
            if len(bills) > 1:
                group = BillGroup(bills=bills, criteria=reason)
                for bill in bills:
                    bill.group = group

    def read_book(self, *args, **kwargs):
        book = open_book(*args, **kwargs)

        # Add a nice tool to find accounts
        def find_account(name):
            account = None
            try:
                account = book.accounts(fullname=name)
                return account
            except KeyError:
                pass
            try:
                account = book.accounts(name=name)
                return account
            except KeyError:
                pass
            names = AccountInput.get_completions(book, name)
            if names:
                return book.accounts(fullname=names[0])
            return None

        book.find_account = find_account
        self.book = book

    def create_split(self, bill: Bill, account=None):
        main_member_ower = next((ower for ower in bill.owers if ower.is_main), None)
        if not bill.payer.is_main and main_member_ower is None:
            print(
                "{} is not participating, skipping…".format(
                    self.budget.main_member.name
                )
            )
            print("{}\n{}".format(bill.payer, bill.owers))
            return None

        if account is None:
            matched_accounts = self.account_matcher.get_best_matches(bill.what)
            account = self.account_input.ask(matched_accounts)
            print("Matched into {}\n".format(account.fullname))

        total_weight = sum(ower.weight for ower in bill.owers)
        amount = -bill.amount if bill.payer.is_main else 0
        if main_member_ower is not None:
            amount += main_member_ower.weight / total_weight * bill.amount

        amount = amount.quantize(QUANTIZER)
        split = Split(
            value=amount,
            account=account,
            transaction=self.transaction,
            memo="{} - {}".format(bill.date, bill.what),
        )

        self.total_transaction_amount += amount
        bill.matched = True
        return split

    def import_budget(self):
        self.ask_missing()
        self.read_budget()

        # Call it for main member name
        self.ask_missing()
        self.budget.set_main_member(self.main_member_name)

        self.read_book(self.gnucash_file, readonly=False)
        self.account_matcher = AccountMatcher(self.book)
        self.account_input = AccountInput(self.book)

        # Call it for main account
        self.ask_missing()
        self.main_account = self.book.find_account(self.main_account_name)

        self.printer = BudgetPrinter(self.budget, self.book)

        bills = self.budget.get_bills()
        self.group_bills()

        self.printer.show_budget_summary()

        self.transaction = Transaction(
            currency=self.main_account.commodity,
            description=self.transaction_description,
        )

        for index, bill in enumerate(bills):
            if bill.group is not None:
                if bill.matched:
                    continue
                self.printer.print_bill_group(bill.group)
                split = self.create_split(bill)
                if split is not None:
                    for grouped_bill in bill.group.bills:
                        if grouped_bill == bill:
                            continue
                        self.create_split(grouped_bill, split.account)
            else:
                self.printer.print_bill_summary(bill, index, len(bills))
                self.create_split(bill)

        Split(
            value=-self.total_transaction_amount,
            account=self.main_account,
            transaction=self.transaction,
        )
        print("Saving… ", end="")
        self.book.save()
        self.book.close()
        print("Done")


@click.command()
@click.argument("gnucash_file")
@click.argument("url")
@click.option("--main-member-name")
@click.option("--main-account-name")
@click.option("--password")
def import_budget(*args, **kwargs):
    importer = IHateGnuCash(*args, **kwargs)
    importer.import_budget()


if __name__ == "__main__":
    import_budget()
